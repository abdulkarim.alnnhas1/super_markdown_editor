library super_markdown_editor;

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:super_markdown_editor/quill_utils.dart';
export 'src/super_markdown_editor_widget.dart';
export 'src/editor_string_res.dart';

class MarkdownFile{
  final String url;
  final String? name;



  MarkdownFile({required this.url,this.name});
}

String markdownToPlainText(String md) {
  md = _handleTable(md);

  /// Replace file markdown with plain text
  md = md.replaceAllMapped(
    RegExp(r'📎 \[([^\]]+)\]\(([^)]+)\)', multiLine: true),
    (match) {
      final linkText = match.group(1);
      return "$linkText";
    },
  );

  /// Replace image markdown with plain text
  md = md.replaceAllMapped(
    RegExp(r'!\[(.*?)\]\(.*?\)', multiLine: true),
    (match) {
      return match.group(1) ?? "";
    },
  );

  Document doc = QuillUtils.convertMarkdownToDocument(md, 12, "ltr");

  final QuillController controller = QuillController(
      keepStyleOnNewLine: true,
      document: doc,
      selection: TextSelection.collapsed(offset: doc.length - 1));

  String markdown = controller.document.getPlainText(0, controller.document.length - 1);

  /// Replace inline code blocks with plain text
  markdown = markdown.replaceAllMapped(RegExp('`[^`]+`', multiLine: true), (match) {
    final codeBlock = match.group(0) ?? "";
    final plainText = codeBlock.replaceAll('`', ''); // Remove the triple backticks
    return plainText;
  });

  /// Remove Vertical Spaces lines
  markdown = markdown.replaceAllMapped(RegExp(r'\n+'), (match) {
    final matchText = match.group(0) ?? '';
    return matchText.startsWith('\n') ? '\n ' : ' ';
  });
  markdown = markdown.replaceAll(RegExp(r'\n '), '\n');

  return markdown;
}

String htmlToPlainText(String html) {
  return html.replaceAll(RegExp(r'<[^>]*>|&[^;]+;'), '');
}

bool isHtmlString(String str) {
  final RegExp htmlRegExp =
  RegExp('<[^>]*>', multiLine: true, caseSensitive: false);
  return htmlRegExp.hasMatch(str);
}



String _handleTable(String markdown) {
  final lines = markdown.split('\n');
  var newLines = <String>[];
  bool insideTable = false;

  for (final line in lines) {
    if (line.startsWith('|') && line.endsWith('|')) {
      if (!insideTable) {
        insideTable = true;
      }
      if (line.replaceAll("-", "").replaceAll("|", "").trim().isEmpty) {
        newLines.add("\n");
      } else {
        newLines.add(line.replaceAll("|", ""));
      }
    } else {
      if (insideTable) insideTable = false;
      newLines.add("");
      newLines.add(line);
    }
  }

  newLines = newLines.map((element) {
    return element.replaceAll(RegExp(r'\s+'), ' ');
  }).toList();
  return newLines.join("\n");
}
