import 'package:html/dom.dart';

import 'package:super_markdown_editor/flutter-quill/lib/src/models/quill_delta.dart';

import 'custom_html_part.dart';

class CustomCodePart extends CustomHtmlPart {
  @override
  List<Operation> convert(Element element, {Map<String, dynamic>? currentAttributes}) {
    return [
      Operation.insert(element.text, {"code": true}),
    ];
  }

  @override
  bool matches(Element element) {
    return element.localName == 'code';
  }
}
