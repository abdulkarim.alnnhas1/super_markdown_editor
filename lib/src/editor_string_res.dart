import 'package:flutter_quill/flutter_quill.dart';

class EditorStringRes {
  final String? undo;
  final String? redo;
  final String? fontFamily;
  final String? fontSize;
  final String? bold;
  final String? subscript;
  final String? superscript;
  final String? italic;
  final String? small;
  final String? underline;
  final String? strikeThrough;
  final String? inlineCode;
  final String? fontColor;
  final String? backgroundColor;
  final String? clearFormat;
  final String? leftAlignment;
  final String? centerAlignment;
  final String? rightAlignment;
  final String? justifyAlignment;
  final String? textDirection;
  final String? headerStyle;
  final String? listNumbers;
  final String? listBullets;
  final String? listChecks;
  final String? codeBlock;
  final String? quote;
  final String? indentIncrease;
  final String? indentDecrease;
  final String? insertLink;
  final String? clear;
  final String? search;

  EditorStringRes(
      {this.undo,
      this.redo,
      this.fontFamily,
      this.fontSize,
      this.bold,
      this.subscript,
      this.superscript,
      this.italic,
      this.small,
      this.underline,
      this.strikeThrough,
      this.inlineCode,
      this.fontColor,
      this.backgroundColor,
      this.clearFormat,
      this.leftAlignment,
      this.centerAlignment,
      this.rightAlignment,
      this.justifyAlignment,
      this.textDirection,
      this.headerStyle,
      this.listNumbers,
      this.listBullets,
      this.listChecks,
      this.codeBlock,
      this.quote,
      this.indentIncrease,
      this.indentDecrease,
      this.insertLink,
      this.clear,
      this.search});

  StringRes toQuill() {
    return StringRes(
        bold: bold,
        italic: italic,
        underline: underline,
        strikeThrough: strikeThrough,
        subscript: subscript,
        superscript: superscript,
        fontColor: fontColor,
        backgroundColor: backgroundColor,
        clearFormat: clearFormat,
        leftAlignment: leftAlignment,
        centerAlignment: centerAlignment,
        rightAlignment: rightAlignment,
        justifyAlignment: justifyAlignment,
        textDirection: textDirection,
        headerStyle: headerStyle,
        listNumbers: listNumbers,
        listBullets: listBullets,
        listChecks: listChecks,
        codeBlock: codeBlock,
        quote: quote,
        indentIncrease: indentIncrease,
        insertLink: insertLink,
        fontFamily: fontFamily,
        inlineCode: inlineCode,
        fontSize: fontSize,
        clear: clear,
        small: small,
        indentDecrease: indentDecrease,
        redo: redo,
        search: search,
        undo: undo);
  }
}
