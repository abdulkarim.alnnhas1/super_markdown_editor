import 'dart:convert';
import 'package:flutter_quill/flutter_quill.dart';

class FileBlockEmbed extends CustomBlockEmbed {
  static const String fileType = 'file';

  final String fileUrl;
  final String fileName;

  FileBlockEmbed({required this.fileUrl, required this.fileName})
      : super(fileType, '📎 [$fileName]($fileUrl)');


}
