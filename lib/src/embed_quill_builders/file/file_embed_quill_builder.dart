import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart' hide Text;
import 'package:super_markdown_editor/src/embed_quill_builders/file/file_block_embed.dart';

class FileEmbedQuillBuilder extends EmbedBuilder {

  final  Function(String url) onDownloadClick;
  final TextStyle textStyle;

  FileEmbedQuillBuilder({required this.textStyle, required this.onDownloadClick});

  @override
  String get key => FileBlockEmbed.fileType;

  @override
  bool get expanded => false;

  @override
  Widget build(
    BuildContext context,
    QuillController controller,
    Embed node,
    bool readOnly,
    bool inline,
    TextStyle textStyle,
  ) {
    var file = node.value.data;
    return _buildFileWidget(context, (file as String).split("(").last.replaceAll(")", ""),(file)
        .split("]").first.replaceAll("[", ""));
  }

  Widget _buildFileWidget(BuildContext context, String fileUrl,String fileName) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
        child: InkWell(
          onTap: (){ onDownloadClick(fileUrl);},
          child: Text(fileName, style: TextStyle(fontWeight: FontWeight.bold,color:
          Colors.blue,fontSize:textStyle.fontSize!-2.0 ,fontFamily: textStyle.fontFamily),
              maxLines: 3),
        ),
      )

    ],);
  }
}
