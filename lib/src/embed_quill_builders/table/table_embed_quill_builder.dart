import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_quill/flutter_quill.dart' as quill;
import 'package:super_markdown_editor/src/editor_string_res.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/table/table_block_embed.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/table/table_builder_widget.dart';

class TableEmbedQuillBuilder extends quill.EmbedBuilder {
  final TableBlockEmbed? passedTable;
  final Function(BuildContext, Widget, VoidCallback) showTableBuilder;
  final Color primaryColor;
  final TextStyle textStyle;
  final EditorStringRes stringRes;
  final String boldAttributeIconPath;
  final String italicAttributeIconPath;
  final String strikeThroughAttributeIconPath;
  final String codeAttributeIconPath;
  final String quoteAttributeIconPath;
  final bool isHtml;
  final String listAttributeIconPath;
  final String numberedListAttributeIconPath;
  final String linkAttributeIconPath;
  final String clearFormatAttributeIconPath;
  final bool viewMode;

  TableEmbedQuillBuilder(
      {required this.boldAttributeIconPath,
      required this.italicAttributeIconPath,
      required this.strikeThroughAttributeIconPath,
      required this.codeAttributeIconPath,
      required this.quoteAttributeIconPath,
      required this.listAttributeIconPath,
      required this.numberedListAttributeIconPath,
      required this.linkAttributeIconPath,
      required this.clearFormatAttributeIconPath,
      required this.textStyle,
      required this.stringRes,
      required this.primaryColor,
      required this.isHtml,
      required this.showTableBuilder,
      required this.viewMode,
      required this.passedTable});

  @override
  String get key => TableBlockEmbed.tableType;

  @override
  bool get expanded => false;

  @override
  Widget build(
    BuildContext context,
    quill.QuillController controller,
    quill.Embed node,
    bool readOnly,
    bool inline,
    TextStyle textStyle,
  ) {
    final TableBlockEmbed tableData = passedTable ?? TableBlockEmbed.fromString(node.value.data);
    final tableWidget = buildTableWidget(tableData.getTableData(), context, controller, node);

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black),
        ),
        child: tableWidget,
      ),
    );
  }

  void _editTable(
      List<List<String>> tableData, quill.QuillController controller, quill.Embed node) {
    final tableBlockEmbed = TableBlockEmbed(tableData);
    controller.replaceText(node.documentOffset, node.length, tableBlockEmbed, null);
  }

  Widget buildTableWidget(List<List<String>> tableData, BuildContext context,
      quill.QuillController controller, quill.Embed node) {
    return InkWell(
      onTap: () {
        if(viewMode == false){
        List<List<String>> table = [];
        showTableBuilder(
          context,
          TableBuilderWidget(
            initialValue: tableData,
            textStyle: textStyle,
            primaryColor: primaryColor,
            stringRes: stringRes,
            strikeThroughAttributeIconPath: strikeThroughAttributeIconPath,
            boldAttributeIconPath: boldAttributeIconPath,
            italicAttributeIconPath: italicAttributeIconPath,
            listAttributeIconPath: listAttributeIconPath,
            isHtml: isHtml,
            numberedListAttributeIconPath: numberedListAttributeIconPath,
            quoteAttributeIconPath: quoteAttributeIconPath,
            linkAttributeIconPath: linkAttributeIconPath,
            codeAttributeIconPath: codeAttributeIconPath,
            clearFormatAttributeIconPath: clearFormatAttributeIconPath,
            onTableDataChanged: (tableData) {
              table = tableData;
            },
          ),
          () {
            if (table.isNotEmpty) {
              _editTable(table, controller, node);
            }
            Navigator.pop(context);
          },
        );}
      },
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Table(
          defaultColumnWidth: const FixedColumnWidth(110),
          border: TableBorder.all(),
          children: List<TableRow>.generate(tableData.length, (rowIndex) {
            return TableRow(
              decoration: rowIndex == 0 ? BoxDecoration(color: Colors.grey.shade300) : null,
              children: List<Widget>.generate(tableData.first.length, (colIndex) {
                return TableCell(
                  child: Column(
                    children: [
                      Markdown(
                        data: tableData[rowIndex][colIndex],
                        shrinkWrap: true,
                        // Allow text to expand vertically based on content
                        // Wrap in SingleChildScrollView if content can be scrollable
                        // Adjust other properties as needed
                        styleSheet: MarkdownStyleSheet.fromTheme(Theme.of(context))
                            .copyWith(textScaleFactor: 1.0),
                      ),
                    ],
                  ),
                );
              }),
            );
          }),
        ),
      ),
    );
  }
}
