import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/file/file_quill_button.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/table/table_block_embed.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/table/table_builder_widget.dart';
import 'package:super_markdown_editor/super_markdown_editor.dart';

class TableQuillButton extends StatelessWidget {
  const TableQuillButton({
    required this.icon,
    required this.controller,
    required this.isHtml,
    required this.stringRes,
    this.iconSize = kDefaultIconSize,
    this.fillColor,
    this.iconTheme,
    this.dialogTheme,
    this.tooltip,
    this.linkRegExp,
    Key? key,
    required this.showTableBuilder,
    required this.primaryColor,
    required this.textStyle,
    required this.boldAttributeIconPath,
    required this.italicAttributeIconPath,
    required this.strikeThroughAttributeIconPath,
    required this.codeAttributeIconPath,
    required this.quoteAttributeIconPath,
    required this.listAttributeIconPath,
    required this.numberedListAttributeIconPath,
    required this.linkAttributeIconPath,
    required this.clearFormatAttributeIconPath,
  }) : super(key: key);

  final String icon;
  final Color primaryColor;
  final EditorStringRes stringRes;
  final String boldAttributeIconPath;
  final String italicAttributeIconPath;
  final String strikeThroughAttributeIconPath;
  final String codeAttributeIconPath;
  final String quoteAttributeIconPath;
  final String listAttributeIconPath;
  final String numberedListAttributeIconPath;
  final String linkAttributeIconPath;
  final String clearFormatAttributeIconPath;
  final double iconSize;
  final bool isHtml;
  final TextStyle textStyle;
  final Color? fillColor;
  final QuillController controller;
  final QuillIconTheme? iconTheme;
  final QuillDialogTheme? dialogTheme;
  final String? tooltip;
  final RegExp? linkRegExp;
  final Function(BuildContext, Widget, VoidCallback) showTableBuilder;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final iconColor = iconTheme?.iconUnselectedColor ?? theme.iconTheme.color;
    final iconFillColor = iconTheme?.iconUnselectedFillColor ?? (fillColor ?? theme.canvasColor);

    return QuillIconButton(
      icon: SvgIconWrapper(
        size: 20,
        iconPath: icon,color: context.isDark?
      Colors.white:null
      ),
      tooltip: tooltip,
      highlightElevation: 0,
      hoverElevation: 0,
      size: iconSize * 1.77,
      fillColor: iconFillColor,
      borderRadius: iconTheme?.borderRadius ?? 2,
      onPressed: () {
        _onPressedHandler(context);
      },
    );
  }

  void _onPressedHandler(BuildContext context) {
    List<List<String>> table = [];
    showTableBuilder(
      context,
      TableBuilderWidget(
        strikeThroughAttributeIconPath: strikeThroughAttributeIconPath,
        boldAttributeIconPath: boldAttributeIconPath,
        isHtml: isHtml,
        stringRes: stringRes,
        italicAttributeIconPath: italicAttributeIconPath,
        listAttributeIconPath: listAttributeIconPath,
        numberedListAttributeIconPath: numberedListAttributeIconPath,
        quoteAttributeIconPath: quoteAttributeIconPath,
        linkAttributeIconPath: linkAttributeIconPath,
        codeAttributeIconPath: codeAttributeIconPath,
        clearFormatAttributeIconPath: clearFormatAttributeIconPath,
        textStyle: textStyle,
        onTableDataChanged: (tableData) {
          table = tableData;
        },
        primaryColor: primaryColor,
      ),
      () {
        if (table.isNotEmpty) {
          _insertTable(table);
        }
        Navigator.pop(context);
      },
    );
  }

  void _insertTable(List<List<String>> tableData) {
    final tableBlockEmbed = TableBlockEmbed(tableData);
    controller.replaceText(controller.selection.baseOffset, 0, tableBlockEmbed, null);
    final imagePosition = controller.document.length - 1;
    controller.updateSelection(TextSelection.collapsed(offset: imagePosition), ChangeSource.LOCAL);
  }
}
