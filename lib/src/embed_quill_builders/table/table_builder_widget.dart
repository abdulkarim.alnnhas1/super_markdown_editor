import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:super_markdown_editor/quill_utils.dart';
import 'package:super_markdown_editor/src/editor_string_res.dart';
import 'package:super_markdown_editor/src/super_markdown_editor_widget.dart';

class TableBuilderWidget extends StatefulWidget {
  final Function(List<List<String>>) onTableDataChanged;
  final List<List<String>>? initialValue; // Make it optional by using the nullable type
  final Color primaryColor;
  final TextStyle textStyle;
  final EditorStringRes stringRes;
  final bool isHtml;
  final String boldAttributeIconPath;
  final String italicAttributeIconPath;
  final String strikeThroughAttributeIconPath;
  final String codeAttributeIconPath;
  final String quoteAttributeIconPath;
  final String listAttributeIconPath;
  final String numberedListAttributeIconPath;
  final String linkAttributeIconPath;
  final String clearFormatAttributeIconPath;

  const TableBuilderWidget({
    Key? key,
    required this.onTableDataChanged,
    required this.stringRes,
    this.initialValue,
    required this.primaryColor,
    required this.isHtml,
    required this.textStyle,
    required this.boldAttributeIconPath,
    required this.italicAttributeIconPath,
    required this.strikeThroughAttributeIconPath,
    required this.codeAttributeIconPath,
    required this.quoteAttributeIconPath,
    required this.listAttributeIconPath,
    required this.numberedListAttributeIconPath,
    required this.linkAttributeIconPath,
    required this.clearFormatAttributeIconPath, // Mark it as optional
  }) : super(key: key);

  @override
  _TableBuilderWidgetState createState() => _TableBuilderWidgetState();
}

class _TableBuilderWidgetState extends State<TableBuilderWidget> {
  late int numRows; // Initial number of rows
  late int numCols; // Initial number of columns
  List<List<TextEditingController>> cellControllers = [];
  List<List<QuillController>> cellQuillControllers = [];
  List<List<FocusNode>> cellFocus = [];
  QuillController? selectedQuil;

  @override
  void initState() {
    super.initState();
    numRows = widget.initialValue?.length ?? 3;
    numCols = widget.initialValue?.first.length ?? 3;
  }

  @override
  void didChangeDependencies() {
    _initializeCellControllers();
    super.didChangeDependencies();
  }

  void _initializeCellControllers() {
    final oldControllers = cellControllers;
    final oldQuillControllers = cellQuillControllers;
    final oldFocus = cellFocus;
    cellControllers = [];
    cellQuillControllers = [];
    cellFocus = [];

    for (var row = 0; row < numRows; row++) {
      List<TextEditingController> rowControllers = [];
      for (var col = 0; col < numCols; col++) {
        TextEditingController controller;

        // Check if old controllers exist and match the current row and column
        if (oldControllers.length > row && oldControllers[row].length > col) {
          controller = oldControllers[row][col];
        } else {
          controller = TextEditingController();
          if (widget.initialValue != null &&
              row < widget.initialValue!.length &&
              col < widget.initialValue![row].length) {
            controller.text = widget.initialValue![row][col];
          }
        }

        rowControllers.add(controller);
      }
      cellControllers.add(rowControllers);
    }

    for (var row = 0; row < numRows; row++) {
      List<QuillController> rowControllers = [];
      List<FocusNode> rowFocus = [];
      for (var col = 0; col < numCols; col++) {
        QuillController controller;
        FocusNode focusNode;

        // Check if old controllers exist and match the current row and column
        if (oldControllers.length > row && oldQuillControllers[row].length > col) {
          controller = oldQuillControllers[row][col];
        } else {
          controller = QuillController.basic();
          if (cellControllers[row][col].text.isNotEmpty) {
            Document doc = widget.isHtml
                ? QuillUtils.convertHtmToDocument(cellControllers[row][col].text)
                : QuillUtils.convertMarkdownToDocument(cellControllers[row][col].text, 10,
                    Directionality.of(context) == TextDirection.ltr ? "ltr" : "rtl");
            controller = QuillController(
                document: doc, selection: TextSelection.collapsed(offset: doc.length - 1));
          }
        }

        // Check if old controllers exist and match the current row and column
        if (oldFocus.length > row && oldFocus[row].length > col) {
          focusNode = oldFocus[row][col];
        } else {
          focusNode = FocusNode();
          focusNode.addListener(() {
            if (focusNode.hasFocus) {
              setState(() {
                selectedQuil = cellQuillControllers[row][col];
              });
            } else {}
          });
        }
        rowControllers.add(controller);
        rowFocus.add(focusNode);
      }
      cellQuillControllers.add(rowControllers);
      cellFocus.add(rowFocus);
    }
  }

  void _updateTableSize(int newRows, int newCols) {
    setState(() {
      numRows = newRows;
      numCols = newCols;
      _initializeCellControllers();
    });
    _buildTableData();
  }

  void _buildTableData() {
    final tableData = cellControllers.map((rowControllers) {
      return rowControllers.map((controller) => controller.text.replaceAll("\n", " ")).toList();
    }).toList();
    widget.onTableDataChanged(tableData);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const Icon(Icons.table_rows, size: 25),
                const SizedBox(width: 30),
                InkWell(
                  onTap: () {
                    _updateTableSize(numRows + 1, numCols);
                  },
                  child: Container(
                    decoration: BoxDecoration(shape: BoxShape.circle, color: widget.primaryColor),
                    child: const Padding(
                      padding: EdgeInsets.all(2.0),
                      child: Icon(Icons.add, size: 20, color: Colors.white),
                    ),
                  ),
                ),
                const SizedBox(width: 20),
                InkWell(
                  onTap: () {
                    if (numRows > 1) {
                      _updateTableSize(numRows - 1, numCols);
                    }
                  },
                  child: Container(
                    height: 20,
                    decoration: BoxDecoration(),
                    child: Icon(Icons.remove, size: 20),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              children: [
                const Icon(Icons.view_column_rounded, size: 25),
                const SizedBox(width: 30),
                InkWell(
                  onTap: () {
                    _updateTableSize(numRows, numCols + 1);
                  },
                  child: Container(
                    decoration: BoxDecoration(shape: BoxShape.circle, color: widget.primaryColor),
                    child: const Padding(
                      padding: EdgeInsets.all(2.0),
                      child: Icon(Icons.add, size: 20, color: Colors.white),
                    ),
                  ),
                ),
                const SizedBox(width: 20),
                InkWell(
                  onTap: () {
                    if (numCols > 2) {
                      _updateTableSize(numRows, numCols - 1);
                    }
                  },
                  child: Container(
                    height: 20,
                    decoration: BoxDecoration(),
                    child: Icon(Icons.remove, size: 20),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20),
            if (selectedQuil != null)
              QuillToolbar.basic(
                controller: selectedQuil!,
                stringRes: widget.stringRes.toQuill(),
                strikeThroughAttributeIconPath: widget.strikeThroughAttributeIconPath,
                boldAttributeIconPath: widget.boldAttributeIconPath,
                underLineAttributeIconPath: widget.boldAttributeIconPath,
                italicAttributeIconPath: widget.italicAttributeIconPath,
                listAttributeIconPath: widget.listAttributeIconPath,
                numberedListAttributeIconPath: widget.numberedListAttributeIconPath,
                quoteAttributeIconPath: widget.quoteAttributeIconPath,
                linkAttributeIconPath: widget.linkAttributeIconPath,
                codeAttributeIconPath: widget.codeAttributeIconPath,
                clearFormatAttributeIconPath: widget.clearFormatAttributeIconPath,
                iconTheme: QuillIconTheme(
                  iconSelectedFillColor: widget.primaryColor,
                ),
                showAlignmentButtons: false,
                showBackgroundColorButton: false,
                showLink: false,
                showSubscript: false,
                showSuperscript: false,
                showInlineCode: false,
                showUndo: false,
                showRedo: false,
                showUnderLineButton: false,
                showListNumbers: false,
                showListBullets: false,
                showSearchButton: false,
                showClearFormat: false,
                showColorButton: false,
                showFontFamily: false,
                showFontSize: false,
                showCodeBlock: true,
                showQuote: false,
                showIndent: false,
                showDividers: false,
                showListCheck: false,
                multiRowsDisplay: false,
              ),
            const SizedBox(height: 20),
            // ConstrainedBox(
            //   constraints: const BoxConstraints(minHeight: 200, maxHeight: 300),
            //   child: SingleChildScrollView(
            //     scrollDirection: Axis.vertical,
            //     child: SingleChildScrollView(
            //       scrollDirection: Axis.horizontal,
            //       child: Column(
            //         mainAxisSize: MainAxisSize.min,
            //         children: List<Widget>.generate(numRows, (row) {
            //           return Container(
            //             decoration: BoxDecoration(border: Border.all(color: Colors.black,width: 0.5)),
            //             child: Row(
            //               children: List<Widget>.generate(numCols, (col) {
            //                 return SizedBox(
            //                   width:90,
            //                   child: Center(
            //                     child: SimpleMarkdownEditor(
            //                       controller: cellQuillControllers[row][col],
            //                       textController: cellControllers[row][col],
            //                       onChanged: (s) {
            //                         _buildTableData();
            //                       },
            //                       textStyle: widget.textStyle,
            //                       focusNode: cellFocus[row][col],
            //                       onLaunchUrl: (String value) {},
            //                     ),
            //                   ),
            //                 );
            //               }),
            //             ),
            //           );
            //         }),
            //       ),
            //     ),
            //   ),
            // ),
            ConstrainedBox(
              constraints: const BoxConstraints(minHeight: 200, maxHeight: 280),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Table(
                    defaultColumnWidth: FixedColumnWidth(110),
                    border: TableBorder.all(),
                    children: List<TableRow>.generate(numRows, (row) {
                      return TableRow(
                        children: List<Widget>.generate(numCols, (col) {
                          return TableCell(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                              child: SimpleMarkdownEditor(
                                isHtml: widget.isHtml,
                                controller: cellQuillControllers[row][col],
                                textController: cellControllers[row][col],
                                onChanged: (s) {
                                  _buildTableData();
                                },
                                textStyle: widget.textStyle,
                                focusNode: cellFocus[row][col],
                                onLaunchUrl: (String value) {},
                              ),
                            ),
                          );
                        }),
                      );
                    }),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
