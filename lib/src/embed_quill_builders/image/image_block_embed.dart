import 'dart:convert';
import 'package:flutter_quill/flutter_quill.dart';

class ImageBlockEmbed extends CustomBlockEmbed {
  static const String imageType = 'image';

  final String fileUrl;
  final String fileName;

  ImageBlockEmbed({required this.fileUrl, required this.fileName})
      : super(imageType, '![$fileName]($fileUrl)');

}
