import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_quill/flutter_quill.dart' hide Text;
import 'package:flutter_quill/translations.dart';
import 'package:flutter_quill/extensions.dart' as base;
import 'package:super_markdown_editor/src/embed_quill_builders/image/image_block_embed.dart';
import 'widgets/image.dart';
import 'widgets/image_resizer.dart';

class ImageEmbedQuillBuilder extends EmbedBuilder {
  ImageEmbedQuillBuilder({required this.viewMode,required this.httpHeaders});

  @override
  String get key => ImageBlockEmbed.imageType;

  @override
  bool get expanded => false;

  final bool viewMode;
  final  Map<String, String> httpHeaders;

  @override
  Widget build(
    BuildContext context,
    QuillController controller,
    Embed node,
    bool readOnly,
    bool inline,
    TextStyle textStyle,
  ) {
    var image;
    final imageUrl = standardizeImageUrl(node.value.data.split("(").last.replaceAll(")", "")
        .replaceAll("}", ""));
    String? size;
    if(imageUrl.split("?").last.startsWith("size="))
      {
        size = imageUrl.split("?").last.replaceAll("size=", "");
      }
    double? height = size!=null?double.parse(size.split("x").last):null;
    double? width  = size!=null?double.parse(size.split("x").first):null;
    OptionalSize? imageSize;
    final style = node.style.attributes['style'];
    if (style != null) {
      final attrs = base.parseKeyValuePairs(style.value.toString(), {
        Attribute.mobileWidth,
        Attribute.mobileHeight,
        Attribute.mobileMargin,
        Attribute.mobileAlignment
      });
      if (attrs.isNotEmpty) {
        assert(attrs[Attribute.mobileWidth] != null && attrs[Attribute.mobileHeight] != null,
            'mobileWidth and mobileHeight must be specified');
        final w = width??double.parse(attrs[Attribute.mobileWidth]!);
        final h = height??double.parse(attrs[Attribute.mobileHeight]!);
        imageSize = OptionalSize(w, h);
        final m = attrs[Attribute.mobileMargin] == null
            ? 0.0
            : double.parse(attrs[Attribute.mobileMargin]!);
        final a = base.getAlignment(attrs[Attribute.mobileAlignment]);
        image = Padding(
            padding: EdgeInsets.all(m),
            child: imageByUrl(imageUrl, width: width??w, height: height??h, alignment: a,
                httpHeaders: httpHeaders));
      }
    }

    if (imageSize == null)  {
      image = imageByUrl(imageUrl,width: width,height: height,httpHeaders: httpHeaders);
      imageSize = OptionalSize(width??(image as CachedNetworkImage).width, height??image.height);
    }


    String _getImageUrlWithNewSize(double width,double height){
      String pureUrl = imageUrl;
      if(imageUrl.split("?").last.startsWith("size=")){
        List<String> tem = imageUrl.split("?");
        tem.removeLast();
        pureUrl = "${tem.join("?")}?size=${width}x$height";
      } else {
        pureUrl = "$pureUrl?size=${width}x$height";
      }

      return pureUrl;
    }

    if (!readOnly && base.isMobile()) {
      return GestureDetector(
          onTap: () {
            if(viewMode == false) {
              showDialog(
                context: context,
                builder: (context) {
                  final resizeOption = _SimpleDialogItem(
                    icon: Icons.settings_outlined,
                    color: Colors.lightBlueAccent,
                    text: 'Resize'.i18n,
                    onPressed: () {
                      Navigator.pop(context);
                      showCupertinoModalPopup<void>(
                          context: context,
                          builder: (context) {
                            final screenSize = MediaQuery.of(context).size;
                            return ImageResizer(
                                onImageResize: (w, h) {
                                  final res = getEmbedNode(controller, controller.selection.start);
                                  final attr = base.replaceStyleString(
                                      getImageStyleString(controller), w, h);
                                  controller.replaceText(
                                      res.offset, res.length??1, BlockEmbed.image(_getImageUrlWithNewSize(w,h)),
                                      TextSelection.collapsed
                                        (offset: res.offset),ignoreFocus: true);
                                  controller
                                    ..skipRequestKeyboard = true
                                    ..formatText(res.offset, 1, StyleAttribute(attr));

                                },
                                imageWidth: imageSize?.width,
                                imageHeight: imageSize?.height,
                                maxWidth: screenSize.width,
                                maxHeight: screenSize.height);
                          });
                    },
                  );
                  final copyOption = _SimpleDialogItem(
                    icon: Icons.copy_all_outlined,
                    color: Colors.cyanAccent,
                    text: 'Copy'.i18n,
                    onPressed: () {
                      final imageNode = getEmbedNode(controller, controller.selection.start).value;
                      final imageUrl = imageNode.value.data;
                      controller.copiedImageUrl =
                          ImageUrl(imageUrl, getImageStyleString(controller));
                      Navigator.pop(context);
                    },
                  );
                  final removeOption = _SimpleDialogItem(
                    icon: Icons.delete_forever_outlined,
                    color: Colors.red.shade200,
                    text: 'Remove'.i18n,
                    onPressed: () {
                      final offset = getEmbedNode(controller, controller.selection.start).offset;
                      controller.replaceText(
                          offset, 1, '', TextSelection.collapsed(offset: offset));
                      Navigator.pop(context);
                    },
                  );
                  return Padding(
                    padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                    child: SimpleDialog(
                        shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10))),
                        children: [resizeOption, copyOption, removeOption]),
                  );
                });
            }
          },
          child: image);
    }

    if (!readOnly || !base.isMobile() || isImageBase64(imageUrl)) {
      return image;
    }

    // We provide option menu for mobile platform excluding base64 image
    return _menuOptionsForReadonlyImage(context, imageUrl, image,viewMode);
  }
}

Widget _menuOptionsForReadonlyImage(BuildContext context, String imageUrl, Widget image,bool
viewMode) {
  return GestureDetector(
      onTap: () {
        if(viewMode == false) {
          showDialog(
            context: context,
            builder: (context) {
              // final saveOption = _SimpleDialogItem(
              //   icon: Icons.save,
              //   color: Colors.greenAccent,
              //   text: 'Save'.i18n,
              //   onPressed: () {
              //     imageUrl = appendFileExtensionToImageUrl(imageUrl);
              //     GallerySaver.saveImage(imageUrl).then((_) {
              //       ScaffoldMessenger.of(context)
              //           .showSnackBar(SnackBar(content: Text('Saved'.i18n)));
              //       Navigator.pop(context);
              //     });
              //   },
              // );
              final zoomOption = _SimpleDialogItem(
                icon: Icons.zoom_in,
                color: Colors.cyanAccent,
                text: 'Zoom'.i18n,
                onPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => ImageTapWrapper(imageUrl: imageUrl)));
                },
              );
              return Padding(
                padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                child: SimpleDialog(
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    children: [
                      //saveOption,
                      zoomOption
                    ]),
              );
            });
        }
      },
      child: image);
}

class _SimpleDialogItem extends StatelessWidget {
  const _SimpleDialogItem(
      {required this.icon,
      required this.color,
      required this.text,
      required this.onPressed,
      Key? key})
      : super(key: key);

  final IconData icon;
  final Color color;
  final String text;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return SimpleDialogOption(
      onPressed: onPressed,
      child: Row(
        children: [
          Icon(icon, size: 36, color: color),
          Padding(
            padding: const EdgeInsetsDirectional.only(start: 16),
            child: Text(text, style: const TextStyle(fontWeight: FontWeight.bold)),
          ),
        ],
      ),
    );
  }
}
