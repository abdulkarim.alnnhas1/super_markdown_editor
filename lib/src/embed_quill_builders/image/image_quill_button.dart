import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/file/file_quill_button.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/image/image_block_embed.dart';

import '../../../super_markdown_editor.dart';

class ImageQuillButton extends StatelessWidget {
  const ImageQuillButton({
    required this.icon,
    required this.controller,
    this.iconSize = kDefaultIconSize,
    this.fillColor,
    this.iconTheme,
    this.dialogTheme,
    this.tooltip,
    this.linkRegExp,
    Key? key, required this.onPickImage,
  }) : super(key: key);

  final String icon;
  final double iconSize;
  final Color? fillColor;
  final QuillController controller;
  final QuillIconTheme? iconTheme;
  final QuillDialogTheme? dialogTheme;
  final String? tooltip;
  final RegExp? linkRegExp;
  final Future<MarkdownFile?> Function() onPickImage;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final iconColor = iconTheme?.iconUnselectedColor ?? theme.iconTheme.color;
    final iconFillColor =
        iconTheme?.iconUnselectedFillColor ?? (fillColor ?? theme.canvasColor);

    return QuillIconButton(
      icon: SvgIconWrapper(iconPath: icon, size: 20,color: context.isDark?
      Colors.white:null),
      tooltip: tooltip,
      highlightElevation: 0,
      hoverElevation: 0,
      size: iconSize * 1.77,
      fillColor: iconFillColor,
      borderRadius: iconTheme?.borderRadius ?? 2,
      onPressed: () => _onPressedHandler(context),
    );
  }

  Future<void> _onPressedHandler(BuildContext context) async {
         final MarkdownFile?  image = await onPickImage();
         _linkSubmitted(image?.url,image?.name);
  }


  void _linkSubmitted(String? url,String? name) {
    if (url != null && url.isNotEmpty) {
      final index = controller.selection.baseOffset;
      final length = controller.selection.extentOffset - index;
      controller.replaceText(index, length, ImageBlockEmbed(fileUrl: url, fileName: name??""), null);
    }
    final imagePosition = controller.document.length - 1;
    controller.updateSelection(TextSelection.collapsed(offset: imagePosition),ChangeSource.LOCAL);

  }
}
