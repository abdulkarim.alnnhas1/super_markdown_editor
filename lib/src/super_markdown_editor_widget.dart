import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart' hide Text;
import 'package:flutter_quill/translations.dart';
import 'package:super_markdown_editor/quill_utils.dart';
import 'package:super_markdown_editor/src/my_quill_embed.dart';
import 'package:super_markdown_editor/super_markdown_editor.dart';


class SuperMarkdownEditor extends StatefulWidget {
  final TextEditingController textController;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onLaunchUrl;
  final EditorStringRes stringRes;
  final TextStyle textStyle;
  final EdgeInsetsGeometry padding;
  final double minHeight;
  final BoxDecoration editorDecoration;
  final Future<MarkdownFile?> Function() onPickImage;
  final Future<MarkdownFile?> Function() onPickFile;
  final void Function(String) onDownloadClick;
  final FocusNode focusNode;
  final Color? primaryColor;
  final Widget Function(bool, VoidCallback) linkActionBuilder;
  final Function(BuildContext, Widget, VoidCallback) showTableBuilder;
  final bool viewMode;
  final bool isHtml;
  final String? hintText;
  final TextStyle? hintStyle;
  final String boldAttributeIconPath;
  final String italicAttributeIconPath;
  final String strikeThroughAttributeIconPath;
  final String codeAttributeIconPath;
  final String quoteAttributeIconPath;
  final String listAttributeIconPath;
  final String numberedListAttributeIconPath;
  final String linkAttributeIconPath;
  final String underLineAttributeIconPath;
  final String clearFormatAttributeIconPath;
  final String imageAttributeIconPath;
  final String attachmentAttributeIconPath;
  final String emojiAttributeIconPath;
  final String tableAttributeIconPath;
  final Map<String, String> httpHeaders;

  const SuperMarkdownEditor(
      {Key? key,
      required this.textController,
      required this.textStyle,
      required this.onChanged,
      required this.onPickImage,
      required this.onPickFile,
      required this.onDownloadClick,
      required this.focusNode,
      required this.onLaunchUrl,
      required this.isHtml,
      required this.stringRes,
      required this.showTableBuilder,
      this.primaryColor,
      required this.minHeight,
      required this.editorDecoration,
      required this.linkActionBuilder,
      required this.viewMode,
      this.hintText,
      this.hintStyle,
      required this.boldAttributeIconPath,
      required this.italicAttributeIconPath,
      required this.strikeThroughAttributeIconPath,
      required this.codeAttributeIconPath,
      required this.quoteAttributeIconPath,
      required this.listAttributeIconPath,
      required this.numberedListAttributeIconPath,
      required this.linkAttributeIconPath,
      required this.underLineAttributeIconPath,
      required this.clearFormatAttributeIconPath,
      required this.imageAttributeIconPath,
      required this.attachmentAttributeIconPath,
      required this.emojiAttributeIconPath,
      required this.tableAttributeIconPath,
      required this.padding,
      required this.httpHeaders})
      : super(key: key);

  @override
  State<SuperMarkdownEditor> createState() => SuperMarkdownEditorState();
}

class SuperMarkdownEditorState extends State<SuperMarkdownEditor> {
  late QuillController quillController;
  late TextStyle _baseTextStyle;

  bool showEmoji = false;

  @override
  void initState() {
    widget.focusNode.addListener(() {
      if (quillController.toggledStyle.isEmpty) {
        quillController.formatSelection(Attribute.fromKeyValue('size', widget.textStyle.fontSize));
      }
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    quillController = _buildQuillController();
    if (widget.isHtml) {
      quillController.document.changes.listen((event) {
        String html = QuillUtils.convertDocToHtml(quillController.document);
        widget.onChanged(html);
      });
    } else {
      quillController.document.changes.listen((event) {
        String markdown = QuillUtils.convertDocToMarkdown(quillController.document);
        widget.textController.text = updateMarkdown(markdown);
        widget.onChanged(updateMarkdown(markdown));
      });
    }
    _baseTextStyle =
        TextStyle(fontFamily: widget.textStyle.fontFamily, color: widget.textStyle.color);
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant SuperMarkdownEditor oldWidget) {
    if (widget.textController.text != oldWidget.textController.text) {
      Document? doc;
      if (widget.textController.text.isEmpty) {
        doc = Document();
      } else {
        doc = widget.isHtml
            ? QuillUtils.convertHtmToDocument(widget.textController.text)
            : QuillUtils.convertMarkdownToDocument(
                widget.textController.text,
                widget.textStyle.fontSize ?? 12,
                Directionality.of(context) == TextDirection.rtl ? "rtl" : "ltr");
      }

      quillController.document = doc;
    }
    super.didUpdateWidget(oldWidget);
  }

  String updateMarkdown(String normal) {
    List<String> newLines = [];
    List<String> lines = normal.split("\n");
    for (int i = 0; i < lines.length; i++) {
      String line = lines[i];
      if (line.contains("|") && !line.startsWith("|")) {
        newLines.add(line.replaceFirst("|", "\n|"));
      } else if (line.contains("|") && !line.endsWith("|")) {
        List<String> theNewLine = line.split("|");
        String extra = theNewLine.last;
        theNewLine.removeLast();
        newLines.add("${theNewLine.join("|")}|\n\n$extra");
      } else if (line.startsWith("|") &&
          line.endsWith("|") &&
          i > 0 &&
          i != lines.length - 2 &&
          line.split("|").length != lines[i - 1].split("|").length &&
          line.split("|").length != lines[i + 1].split("|").length) {
        List<String> theNewLine = line.split("|").take(lines[i - 1].split("|").length - 1).toList();
        String extra = line.split("|")[lines[i - 1].split("|").length - 1];
        List<String> newTable = line.split("|");
        newTable.removeRange(0, lines[i - 1].split("|").length);
        newLines.add("${theNewLine.join("|")}|\n\n$extra\n|${newTable.join("|")}");
      } else {
        newLines.add(line);
      }
    }
    return newLines.join("\n");
  }

  QuillController _buildQuillController() {
    QuillController controller;
    Document? doc;
    if (widget.textController.text.isEmpty) {
      controller = QuillController(
          document: Document(),
          keepStyleOnNewLine: true,
          selection: const TextSelection.collapsed(offset: 0));
    } else {
      doc = widget.isHtml
          ? QuillUtils.convertHtmToDocument(widget.textController.text)
          : QuillUtils.convertMarkdownToDocument(
              widget.textController.text,
              widget.textStyle.fontSize ?? 12,
              Directionality.of(context) == TextDirection.rtl ? "rtl" : "ltr");

      controller = QuillController(
          keepStyleOnNewLine: true,
          document: doc,
          selection: TextSelection.collapsed(offset: doc.length - 1));
    }
    if (Directionality.of(context) == TextDirection.rtl) {
      controller.formatSelection(Attribute.fromKeyValue('direction', "rtl"));
    }
    return controller;
  }

  Future<LinkMenuAction> _showMaterialMenu(BuildContext context, String link) async {
    final result = await showModalBottomSheet<LinkMenuAction>(
      context: context,
      builder: (ctx) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _MaterialAction(
              title: 'Open'.i18n,
              icon: Icons.language_sharp,
              style: widget.textStyle.copyWith(fontSize: (widget.textStyle.fontSize ?? 12) + 4.0),
              onPressed: () => Navigator.of(context).pop(LinkMenuAction.launch),
            ),
            _MaterialAction(
              title: 'Copy'.i18n,
              icon: Icons.copy_sharp,
              style: widget.textStyle.copyWith(fontSize: (widget.textStyle.fontSize ?? 12) + 4.0),
              onPressed: () => Navigator.of(context).pop(LinkMenuAction.copy),
            ),
            _MaterialAction(
              title: 'Remove'.i18n,
              icon: Icons.link_off_sharp,
              style: widget.textStyle.copyWith(fontSize: (widget.textStyle.fontSize ?? 12) + 4.0),
              onPressed: () => Navigator.of(context).pop(LinkMenuAction.remove),
            ),
          ],
        );
      },
    );

    return result ?? LinkMenuAction.none;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          decoration: widget.editorDecoration,
          child: QuillEditor(
              enableInteractiveSelection: widget.viewMode == false,
              placeholder: widget.viewMode ? null : widget.hintText,
              minHeight: widget.minHeight,
              controller: quillController,
              showCursor: widget.viewMode ? false : true,
              embedBuilders: MyQuillEmbeds.builders(
                  viewMode: widget.viewMode,
                  isHtml: widget.isHtml,
                  stringRes: widget.stringRes,
                  strikeThroughAttributeIconPath: widget.strikeThroughAttributeIconPath,
                  boldAttributeIconPath: widget.boldAttributeIconPath,
                  italicAttributeIconPath: widget.italicAttributeIconPath,
                  listAttributeIconPath: widget.listAttributeIconPath,
                  numberedListAttributeIconPath: widget.numberedListAttributeIconPath,
                  quoteAttributeIconPath: widget.quoteAttributeIconPath,
                  linkAttributeIconPath: widget.linkAttributeIconPath,
                  codeAttributeIconPath: widget.codeAttributeIconPath,
                  clearFormatAttributeIconPath: widget.clearFormatAttributeIconPath,
                  primaryColor: widget.primaryColor ?? Theme.of(context).primaryColor,
                  showTableBuilder: widget.showTableBuilder,
                  httpHeaders: widget.httpHeaders,
                  baseTextStyle: _baseTextStyle,
                  fullTextStyle: widget.textStyle,
                  onDownloadClick: widget.onDownloadClick),
              customStyles: DefaultStyles(
                sizeSmall: _baseTextStyle,
                placeHolder: widget.hintStyle == null
                    ? null
                    : DefaultTextBlockStyle(widget.hintStyle!, const VerticalSpacing(1, 1),
                        const VerticalSpacing(1, 1), const BoxDecoration()),
                color: _baseTextStyle.color,
                bold: _baseTextStyle.copyWith(fontWeight: FontWeight.bold),
                italic: _baseTextStyle.copyWith(fontStyle: FontStyle.italic),
                link: _baseTextStyle.copyWith(fontWeight: FontWeight.bold, color: Colors.blue),
                underline: _baseTextStyle.copyWith(decoration: TextDecoration.underline),
                strikeThrough: _baseTextStyle.copyWith(decoration: TextDecoration.lineThrough),
                superscript: _baseTextStyle,
                subscript: _baseTextStyle,
                small: _baseTextStyle,
                inlineCode: InlineCodeStyle(
                  style: _baseTextStyle.copyWith(
                      fontFamily: "Rob"
                          "oto Mono",
                      color: Colors.grey.shade600,
                      backgroundColor: Colors.transparent),
                  radius: const Radius.circular(10),
                ),
                h1: DefaultListBlockStyle(
                    _baseTextStyle.copyWith(fontSize: ((_baseTextStyle.fontSize ?? 12) + 12)),
                    const VerticalSpacing(12, 12),
                    const VerticalSpacing(12, 12),
                    const BoxDecoration(),
                    null),
                h2: DefaultListBlockStyle(
                    _baseTextStyle.copyWith(fontSize: ((_baseTextStyle.fontSize ?? 12) + 6)),
                    const VerticalSpacing(6, 6),
                    const VerticalSpacing(6, 6),
                    const BoxDecoration(),
                    null),
                h3: DefaultListBlockStyle(
                    _baseTextStyle.copyWith(fontSize: ((_baseTextStyle.fontSize ?? 12) + 2)),
                    const VerticalSpacing(2, 2),
                    const VerticalSpacing(2, 2),
                    const BoxDecoration(),
                    null),
                lists: DefaultListBlockStyle(_baseTextStyle, const VerticalSpacing(4, 4),
                    const VerticalSpacing(4, 4), const BoxDecoration(), null),
                leading: DefaultListBlockStyle(_baseTextStyle, const VerticalSpacing(4, 4),
                    const VerticalSpacing(4, 4), const BoxDecoration(), null),
                paragraph: DefaultListBlockStyle(_baseTextStyle, const VerticalSpacing(1, 1),
                    const VerticalSpacing(1, 1), const BoxDecoration(), null),
                quote: DefaultListBlockStyle(
                    _baseTextStyle,
                    const VerticalSpacing(1, 1),
                    const VerticalSpacing(1, 1),
                    BoxDecoration(
                        border: BorderDirectional(
                            start: BorderSide(width: 4, color: Colors.grey.shade400))),
                    null),
              ),
              focusNode: widget.focusNode,
              enableUnfocusOnTapOutside: true,
              onLaunchUrl: widget.onLaunchUrl,
              scrollController: ScrollController(),
              scrollable: true,
              padding: widget.padding,
              autoFocus: false,
              linkActionPickerDelegate: (context, link, node) {
                return _showMaterialMenu(context, link);
              },
              readOnly: widget.viewMode,
              expands: false),
        ),
        if (widget.viewMode == false)
          QuillToolbar.basic(
            controller: quillController,
            stringRes: widget.stringRes.toQuill(),
            strikeThroughAttributeIconPath: widget.strikeThroughAttributeIconPath,
            boldAttributeIconPath: widget.boldAttributeIconPath,
            italicAttributeIconPath: widget.italicAttributeIconPath,
            listAttributeIconPath: widget.listAttributeIconPath,
            numberedListAttributeIconPath: widget.numberedListAttributeIconPath,
            quoteAttributeIconPath: widget.quoteAttributeIconPath,
            linkAttributeIconPath: widget.linkAttributeIconPath,
            underLineAttributeIconPath: widget.underLineAttributeIconPath,
            codeAttributeIconPath: widget.codeAttributeIconPath,
            clearFormatAttributeIconPath: widget.clearFormatAttributeIconPath,
            embedButtons: MyQuillEmbeds.buttons(
                isHtml: widget.isHtml,
                stringRes: widget.stringRes,
                imageAttributeIconPath: widget.imageAttributeIconPath,
                attachmentAttributeIconPath: widget.attachmentAttributeIconPath,
                emojiAttributeIconPath: widget.emojiAttributeIconPath,
                tableAttributeIconPath: widget.tableAttributeIconPath,
                strikeThroughAttributeIconPath: widget.strikeThroughAttributeIconPath,
                boldAttributeIconPath: widget.boldAttributeIconPath,
                italicAttributeIconPath: widget.italicAttributeIconPath,
                listAttributeIconPath: widget.listAttributeIconPath,
                numberedListAttributeIconPath: widget.numberedListAttributeIconPath,
                quoteAttributeIconPath: widget.quoteAttributeIconPath,
                linkAttributeIconPath: widget.linkAttributeIconPath,
                codeAttributeIconPath: widget.codeAttributeIconPath,
                clearFormatAttributeIconPath: widget.clearFormatAttributeIconPath,
                primaryColor: widget.primaryColor ?? Theme.of(context).primaryColor,
                textStyle: _baseTextStyle,
                showTableBuilder: widget.showTableBuilder,
                onPickFile: widget.onPickFile,
                onPickImage: widget.onPickImage,
                onEmojiPressed: () {
                  setState(() {
                    showEmoji = !showEmoji;
                  });
                }),
            linkDialogAction: LinkDialogAction(builder: widget.linkActionBuilder),
            iconTheme: QuillIconTheme(
              iconSelectedFillColor: widget.primaryColor,
            ),
            afterButtonPressed: () {
              if (quillController.getSelectionStyle().attributes.keys.contains("header")) {
                quillController.formatSelection(Attribute.fromKeyValue('size', null));
              } else {
                quillController
                    .formatSelection(Attribute.fromKeyValue('size', widget.textStyle.fontSize));
              }
            },
            showAlignmentButtons: widget.isHtml,
            showBackgroundColorButton: widget.isHtml,
            showSubscript: false,
            showSuperscript: false,
            showInlineCode: true,
            showUndo: false,
            showSmallButton: false,
            showRedo: false,
            showSearchButton: false,
            showUnderLineButton: widget.isHtml,
            showClearFormat: true,
            showHeaderStyle: true,
            showColorButton: widget.isHtml,
            showFontFamily: widget.isHtml,
            showQuote: widget.isHtml == false,
            showFontSize: false,
            showCodeBlock: false,
            showIndent: false,
            showDividers: false,
            showListCheck: false,
            multiRowsDisplay: false,
          ),
        if (showEmoji) const SizedBox(height: 20),
        if (showEmoji)
          SizedBox(
            height: 300,
            child: EmojiPicker(
              config: const Config(
                emojiViewConfig: EmojiViewConfig(
                    columns: 8, emojiSizeMax: 30, horizontalSpacing: 10, verticalSpacing: 10),
              ),
              onEmojiSelected: (_, emoji) {
                _insertEmoji(emoji.emoji);
              },
            ),
          )
      ],
    );
  }

  void _insertEmoji(String emoji) {
    _insertText(emoji);
  }

  void _insertText(String text) {
    final index = quillController.selection.baseOffset;
    final length = quillController.selection.extentOffset - index;
    quillController.replaceText(index, length, text, null);
    quillController.updateSelection(
        TextSelection.collapsed(offset: index + text.length), ChangeSource.LOCAL);
  }
}

class SimpleMarkdownEditor extends StatefulWidget {
  final TextEditingController textController;
  final QuillController controller;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onLaunchUrl;
  final bool isHtml;
  final TextStyle textStyle;
  final FocusNode focusNode;
  final Color? primaryColor;

  const SimpleMarkdownEditor(
      {Key? key,
      required this.textController,
      required this.textStyle,
      required this.onChanged,
      required this.focusNode,
      required this.onLaunchUrl,
      required this.isHtml,
      this.primaryColor,
      required this.controller})
      : super(key: key);

  @override
  State<SimpleMarkdownEditor> createState() => _SimpleMarkdownEditorState();
}

class _SimpleMarkdownEditorState extends State<SimpleMarkdownEditor> {
  bool showEmoji = false;
  late TextStyle _baseTextStyle;

  @override
  void initState() {
    if (widget.isHtml) {
      widget.controller.document.changes.listen((event) {
        String html = QuillUtils.convertDocToHtml(widget.controller.document);
        widget.onChanged(html);
      });
    } else {
      widget.controller.document.changes.listen((event) {
        String markdown = QuillUtils.convertDocToMarkdown(widget.controller.document);
        widget.textController.text = markdown;
        widget.onChanged(markdown);
      });
    }
    _baseTextStyle = TextStyle(
      fontFamily: widget.textStyle.fontFamily,
      color: widget.textStyle.color,
    );
    widget.controller.formatSelection(Attribute.fromKeyValue('size', widget.textStyle.fontSize));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        QuillEditor(
            controller: widget.controller,
            focusNode: widget.focusNode,
            onLaunchUrl: widget.onLaunchUrl,
            customStyles: DefaultStyles(
              color: _baseTextStyle.color,
              bold: _baseTextStyle.copyWith(fontWeight: FontWeight.bold),
              italic: _baseTextStyle.copyWith(fontStyle: FontStyle.italic),
              link: _baseTextStyle.copyWith(fontWeight: FontWeight.bold, color: Colors.blue),
              underline: _baseTextStyle.copyWith(decoration: TextDecoration.underline),
              strikeThrough: _baseTextStyle.copyWith(decoration: TextDecoration.lineThrough),
              superscript: _baseTextStyle,
              subscript: _baseTextStyle,
              small: _baseTextStyle,
              h1: DefaultListBlockStyle(
                  _baseTextStyle.copyWith(fontSize: ((_baseTextStyle.fontSize ?? 12) + 12)),
                  const VerticalSpacing(12, 12),
                  const VerticalSpacing(12, 12),
                  const BoxDecoration(),
                  null),
              h2: DefaultListBlockStyle(
                  _baseTextStyle.copyWith(fontSize: ((_baseTextStyle.fontSize ?? 12) + 6)),
                  const VerticalSpacing(6, 6),
                  const VerticalSpacing(6, 6),
                  const BoxDecoration(),
                  null),
              h3: DefaultListBlockStyle(
                  _baseTextStyle.copyWith(fontSize: ((_baseTextStyle.fontSize ?? 12) + 2)),
                  const VerticalSpacing(2, 2),
                  const VerticalSpacing(2, 2),
                  const BoxDecoration(),
                  null),
              lists: DefaultListBlockStyle(_baseTextStyle, const VerticalSpacing(4, 4),
                  const VerticalSpacing(4, 4), const BoxDecoration(), null),
              leading: DefaultListBlockStyle(_baseTextStyle, const VerticalSpacing(4, 4),
                  const VerticalSpacing(4, 4), const BoxDecoration(), null),
              paragraph: DefaultListBlockStyle(_baseTextStyle, const VerticalSpacing(1, 1),
                  const VerticalSpacing(1, 1), const BoxDecoration(), null),
              quote: DefaultListBlockStyle(
                  _baseTextStyle,
                  const VerticalSpacing(1, 1),
                  const VerticalSpacing(1, 1),
                  BoxDecoration(
                      border: BorderDirectional(
                          start: BorderSide(width: 4, color: Colors.grey.shade400))),
                  null),
            ),
            scrollController: ScrollController(),
            scrollable: true,
            padding: const EdgeInsets.all(8),
            autoFocus: false,
            readOnly: false,
            expands: false),
      ],
    );
  }
}

class _MaterialAction extends StatelessWidget {
  const _MaterialAction({
    required this.title,
    required this.icon,
    required this.onPressed,
    Key? key,
    required this.style,
  }) : super(key: key);

  final String title;
  final IconData icon;
  final VoidCallback onPressed;
  final TextStyle style;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 16,
              color: style.color,
            ),
            const SizedBox(
              width: 8,
            ),
            Text(title, style: style),
          ],
        ),
      ),
    );
  }
}
