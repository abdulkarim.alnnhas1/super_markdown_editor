import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/emoji/emoji_embed_quill_builder.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/emoji/emoji_quill_button.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/file/file_embed_quill_builder.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/file/file_quill_button.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/image/image_embed_quill_builder.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/image/image_quill_button.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/table/table_embed_quill_builder.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/table/table_quill_button.dart';

import '../super_markdown_editor.dart';
import 'editor_string_res.dart';

class MyQuillEmbeds {
  static List<EmbedBuilder> builders(
          {required Color primaryColor,
          required Function(String) onDownloadClick,
          required TextStyle baseTextStyle,
          required TextStyle fullTextStyle,
          required EditorStringRes stringRes,
          required bool viewMode,
          required bool isHtml,
          required String boldAttributeIconPath,
          required String italicAttributeIconPath,
          required String strikeThroughAttributeIconPath,
          required String codeAttributeIconPath,
          required String quoteAttributeIconPath,
          required String listAttributeIconPath,
          required String numberedListAttributeIconPath,
          required String linkAttributeIconPath,
          required Map<String, String> httpHeaders,
          required String clearFormatAttributeIconPath,
          required Function(BuildContext, Widget, VoidCallback) showTableBuilder}) =>
      isHtml
          ? []
          : [
              ImageEmbedQuillBuilder(viewMode: viewMode, httpHeaders: httpHeaders),
              FileEmbedQuillBuilder(onDownloadClick: onDownloadClick, textStyle: fullTextStyle),
              TableEmbedQuillBuilder(
                  passedTable: null,
                  isHtml: isHtml,
                  viewMode: viewMode,
                  stringRes: stringRes,
                  strikeThroughAttributeIconPath: strikeThroughAttributeIconPath,
                  boldAttributeIconPath: boldAttributeIconPath,
                  italicAttributeIconPath: italicAttributeIconPath,
                  listAttributeIconPath: listAttributeIconPath,
                  numberedListAttributeIconPath: numberedListAttributeIconPath,
                  quoteAttributeIconPath: quoteAttributeIconPath,
                  linkAttributeIconPath: linkAttributeIconPath,
                  codeAttributeIconPath: codeAttributeIconPath,
                  clearFormatAttributeIconPath: clearFormatAttributeIconPath,
                  showTableBuilder: showTableBuilder,
                  primaryColor: primaryColor,
                  textStyle: baseTextStyle),
              EmojiEmbedQuillBuilder()
            ];

  static List<EmbedButtonBuilder> buttons(
          {required Future<MarkdownFile?> Function() onPickImage,
          required Future<MarkdownFile?> Function() onPickFile,
          required Color primaryColor,
          required bool isHtml,
          required EditorStringRes stringRes,
          required String boldAttributeIconPath,
          required String italicAttributeIconPath,
          required String strikeThroughAttributeIconPath,
          required String codeAttributeIconPath,
          required String quoteAttributeIconPath,
          required String listAttributeIconPath,
          required String numberedListAttributeIconPath,
          required String linkAttributeIconPath,
          required String clearFormatAttributeIconPath,
          required String tableAttributeIconPath,
          required String imageAttributeIconPath,
          required String emojiAttributeIconPath,
          required String attachmentAttributeIconPath,
          required Function(BuildContext, Widget, VoidCallback) showTableBuilder,
          required TextStyle textStyle,
          required VoidCallback onEmojiPressed}) =>
      isHtml
          ? []
          : [
              (controller, toolbarIconSize, iconTheme, dialogTheme) => TableQuillButton(
                    icon: tableAttributeIconPath,
                    isHtml: isHtml,
                    iconSize: toolbarIconSize,
                    stringRes: stringRes,
                    controller: controller,
                    strikeThroughAttributeIconPath: strikeThroughAttributeIconPath,
                    boldAttributeIconPath: boldAttributeIconPath,
                    italicAttributeIconPath: italicAttributeIconPath,
                    listAttributeIconPath: listAttributeIconPath,
                    numberedListAttributeIconPath: numberedListAttributeIconPath,
                    quoteAttributeIconPath: quoteAttributeIconPath,
                    linkAttributeIconPath: linkAttributeIconPath,
                    codeAttributeIconPath: codeAttributeIconPath,
                    clearFormatAttributeIconPath: clearFormatAttributeIconPath,
                    iconTheme: iconTheme,
                    dialogTheme: dialogTheme,
                    primaryColor: primaryColor,
                    textStyle: textStyle,
                    showTableBuilder: showTableBuilder,
                  ),
              (controller, toolbarIconSize, iconTheme, dialogTheme) => ImageQuillButton(
                    icon: imageAttributeIconPath,
                    iconSize: toolbarIconSize,
                    controller: controller,
                    iconTheme: iconTheme,
                    dialogTheme: dialogTheme,
                    onPickImage: onPickImage,
                  ),
              (controller, toolbarIconSize, iconTheme, dialogTheme) => FileQuillButton(
                    icon: attachmentAttributeIconPath,
                    iconSize: toolbarIconSize,
                    controller: controller,
                    iconTheme: iconTheme,
                    dialogTheme: dialogTheme,
                    onPickFile: onPickFile,
                  ),
              (controller, toolbarIconSize, iconTheme, dialogTheme) => EmojiQuillButton(
                    icon: emojiAttributeIconPath,
                    iconSize: toolbarIconSize,
                    controller: controller,
                    iconTheme: iconTheme,
                    dialogTheme: dialogTheme,
                    onEmojiPressed: onEmojiPressed,
                  )
            ];
}
