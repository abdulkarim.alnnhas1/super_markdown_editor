import 'package:flutter_quill/flutter_quill.dart';
import 'package:super_markdown_editor/src/html_quill_converter/parser/custom_code_part.dart';
import 'package:super_markdown_editor/src/html_quill_converter/parser/html_to_delta.dart';
import 'package:super_markdown_editor/src/html_quill_converter/src/op_attribute_sanitizer.dart';
import 'package:super_markdown_editor/src/html_quill_converter/src/quill_delta_to_html_converter.dart';
import 'package:super_markdown_editor/src/markdown_quill_converter/delta_to_markdown.dart';
import 'package:super_markdown_editor/src/markdown_quill_converter/markdown_to_delta.dart';
import 'package:markdown/markdown.dart' as md;

class QuillUtils {
  static final DeltaToMarkdown _deltaToMarkdown = DeltaToMarkdown();

  static String convertDocToMarkdown(Document doc) => _deltaToMarkdown.convert(doc.toDelta());

  static String convertDocToHtml(Document doc) {
    List<Map<String, dynamic>> rawDeltaOps = doc.toDelta().toJson() as List<Map<String, dynamic>>;
    final converter = QuillDeltaToHtmlConverter(
        rawDeltaOps,
        ConverterOptions(
          sanitizerOptions: OpAttributeSanitizerOptions(allow8DigitHexColors: true),
          multiLineCodeblock: true,
        ));
    return converter.convert();
  }

  static Document convertMarkdownToDocument(String markdown, double fontSize, String dir) {
    var mdDocument = md.Document(encodeHtml: false);
    var mdToDelta = MarkdownToDelta(markdownDocument: mdDocument, fontSize: fontSize, dir: dir);
    if (mdToDelta.convert(markdown).isEmpty) {
      return Document();
    }
    return Document.fromDelta(mdToDelta.convert(markdown));
  }

  static Document convertHtmToDocument(String html) {
    var delta = HtmlToDelta(
      customBlocks: [CustomCodePart()]
    ).convert(html);
    var deltaJson = delta.toJson();
    Delta delta2 = Delta.fromJson(deltaJson);
    return Document.fromDelta(delta2);
  }
}

