import 'package:flutter_test/flutter_test.dart';

import 'package:super_markdown_editor/super_markdown_editor.dart' as md;

/// table markdown to plain text
void main() {
  test('Bold Markdown to Plain Text Conversion', () {
    const input = "**Bold**";

    const expectedOutput = "Bold";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('strikethrough Markdown to Plain Text Conversion', () {
    const input = "~~strikethrough~~";

    const expectedOutput = "strikethrough";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('italic Markdown to Plain Text Conversion', () {
    const input = "_italic_";

    const expectedOutput = "italic";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('code Markdown to Plain Text Conversion', () {
    const input = "`code`";

    const expectedOutput = "code";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('multi line code Markdown to Plain Text Conversion', () {
    const input = "`multi\nline\ncode`";

    const expectedOutput = "multi\nline\ncode";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('Heading1 Markdown to Plain Text Conversion', () {
    const input = "# Heading1";

    const expectedOutput = "Heading1";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('Heading2 Markdown to Plain Text Conversion', () {
    const input = "## Heading2";

    const expectedOutput = "Heading2";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('Heading3 Markdown to Plain Text Conversion', () {
    const input = "### Heading3";

    const expectedOutput = "Heading3";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('Numbered List Markdown to Plain Text Conversion', () {
    const input = "1. Line1\n2. Line2\n3. Line3";

    const expectedOutput = "Line1\nLine2\nLine3";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('List Markdown to Plain Text Conversion', () {
    const input = "- Line1\n- Line2\n- Line3";

    const expectedOutput = "Line1\nLine2\nLine3";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('Quote Markdown to Plain Text Conversion', () {
    const input = "> Line1\n> Line2\n> Line3";

    const expectedOutput = "Line1\nLine2\nLine3";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('Image Markdown to Plain Text Conversion', () {
    const input = "![ImageName](https://qa.face.osos.roaa.tech/api/v1/files/download/image1)";

    const expectedOutput = "ImageName";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('file Markdown to Plain Text Conversion', () {
    const input = "📎 [IMG_20230911_204614.jpg](https://qa.face.osos.roaa"
        ".tech/api/v1/files/download/7cadfasca)";

    const expectedOutput = "IMG_20230911_204614.jpg";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('link Markdown to Plain Text Conversion', () {
    const input = "[google](www.google.com)";

    const expectedOutput = "google";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('table 3*3 Markdown to Plain Text Conversion', () {
    const input = "| Name     | Age | City      |\n"
        "|----------|-----|-----------|\n"
        "| John     | 25  | New York  |\n"
        "| Emily    | 32  | London    |\n"
        "| Michael  | 40  | Sydney    |";

    const expectedOutput = "Name Age City\n"
        "John 25 New York\n"
        "Emily 32 London\n"
        "Michael 40 Sydney";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });
  test('table 3*2 Markdown to Plain Text Conversion', () {
    const input = "| Name     | Age |\n"
        "|----------|-----|\n"
        "| John     | 25  |\n"
        "| Emily    | 32  |\n"
        "| Michael  | 40  |";

    const expectedOutput = "Name Age\n"
        "John 25\n"
        "Emily 32\n"
        "Michael 40";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });
  test('table 2*3 Markdown to Plain Text Conversion', () {
    const input = "| Name     | Age | City      |\n"
        "|----------|-----|-----------|\n"
        "| John     | 25  | New York  |\n"
        "| Emily    | 32  | London    |";

    const expectedOutput = "Name Age City\n"
        "John 25 New York\n"
        "Emily 32 London";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });
  test('table 4*4 Markdown to Plain Text Conversion', () {
    const input = "| Name     | Age | City      | Country|\n"
        "|----------|-----|-----------|--------|\n"
        "| John     | 25  | New York  | E      |\n"
        "| Emily    | 32  | London    | A      |\n"
        "| Michael  | 40  | Sydney    | B      |";

    const expectedOutput = "Name Age City Country\n"
        "John 25 New York E\n"
        "Emily 32 London A\n"
        "Michael 40 Sydney B";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('Horizontal spaces Markdown removed', () {
    const input = "Word1      Word2      Word3";

    const expectedOutput = "Word1 Word2 Word3";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('Vertical spaces Markdown removed', () {
    const input = "Word1\n\n\n\n\n      Word2\n      Word3";

    const expectedOutput = "Word1\nWord2\nWord3";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });

  test('full Markdown to Plain Text Conversion', () {
    const input = """
**Bold**
~~strikethrough~~
_italic_
\`code\`
\`multi
line
code\`
# Heading1
## Heading2
### Heading3
1. Line1
2. Line2
3. Line3
- Line1
- Line2
- Line3
> Line1
> Line2
> Line3
![ImageName.jpg](https://qa.face.osos.roaa.tech/api/v1/files/download/image1)
📎 [IMG_20230911_204614.jpg](https://qa.face.osos.roaa.tech/api/v1/files/download/7cadfasca)
[google](www.google.com)
| Name     | Age | City      |
|----------|-----|-----------|
| John     | 25  | New York  |
| Emily    | 32  | London    |
| Michael  | 40  | Sydney    |
| Name     | Age |
|----------|-----|
| John     | 25  |
| Emily    | 32  |
| Michael  | 40  |
| Name     | Age | City      |
|----------|-----|-----------|
| John     | 25  | New York  |
| Emily    | 32  | London    |
| Name     | Age | City      | Country|
|----------|-----|-----------|--------|
| John     | 25  | New York  | E      |
| Emily    | 32  | London    | A      |
| Michael  | 40  | Sydney    | B      |
Word1      Word2      Word3
Word1


Word2      
Word3
  """;

    const expectedOutput = """
Bold
strikethrough
italic
code
multi
line
code
Heading1
Heading2
Heading3
Line1
Line2
Line3
Line1
Line2
Line3
Line1
Line2
Line3
ImageName.jpg
IMG_20230911_204614.jpg
google
Name Age City
John 25 New York
Emily 32 London
Michael 40 Sydney
Name Age
John 25
Emily 32
Michael 40
Name Age City
John 25 New York
Emily 32 London
Name Age City Country
John 25 New York E
Emily 32 London A
Michael 40 Sydney B
Word1 Word2 Word3
Word1
Word2
Word3""";

    final result = md.markdownToPlainText(input);

    expect(result, equals(expectedOutput));
  });
}
